// Exercício 1
// Dava para ser feito com o reduce. Mas como não foi passado ainda...
function sum(...args) {
    let acc = 0;
    for (const arg of args) {
        acc += arg;
    }
    return acc;
}


// Exercício 2
function map(n, arr) {
    return arr.map(x => x*n);
}

// Exercício 3
function filterLetters(filter, stringList) {
    return stringList.filter(str => {
        for (const chr of str) {
            if (!filter.includes(chr)) {
                return false;
            }
        }
        return true;
    });
}

// console.log(filterLetters("ab", ["abc", "ba", "ab", "bb", "kb"]));
// console.log(filterLetters("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]));


// Exercício 4
function intersection(...arrList) {
    let newArr = [...arrList[0]];

    for (let i=1; i < arrList.length; i++) {
        newArr = newArr.filter(x => arrList[i].includes(x));
    }
    
    return newArr;
}

// console.log(intersection([1, 2, 3], [3, 3, 7], [9, 111, 3]));
// console.log(intersection([120, 120, 110, 2], [110, 2, 130]));


// Exercício 5
function evenArrays(...arrList) {
    // De novo, outro exemplo que um reduce cairia como uma luva kkk
    return arrList.filter(arr => {
        let acc = 0;
        for (const value of arr) {
            acc += value;
        }
        return (acc % 2 == 0);
    })
}

// console.log(evenArrays([1, 1, 3], [1, 2, 2, 2, 3], [2]));
// console.log(evenArrays([2, 2, 2, 1], [3, 2, 1]));